/**
 * Author: Zarandi David (Azuwey)
 * Date: 03/08/2019
 * Source: https://gitlab.com/azuwey/protocol-off-grid
 * License: BSD 3-clause "New" or "Revised"
 **/

#include "broadcaster.hpp"

POG::Broadcaster::Broadcaster() {
  // Creating socket file descriptor
  if ((socket_fd = socket(AF_INET6, SOCK_STREAM, 0)) == 0) {
    throw FileDescriptorException();
  }

  // Setup address
  this->address.sin6_family = AF_INET6;
  inet_pton(AF_INET6, "::1", &address.sin6_addr);

  bool isSuccess = false;
  do {
    // Attaching socket to the port
    if (setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt,
                   sizeof(opt)) != 0) {
      throw FileDescriptorBindingException();
    }

    // Attach port to the address
    this->address.sin6_port = htonl(this->port);

    // Bind port to file descriptor
    if (bind(socket_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
      close(socket_fd);
      ++this->port;
    } else {
      isSuccess = true;
    }
  } while (!isSuccess && port < UINT16_MAX);

  // Check binding was successful or not
  if (!isSuccess) {
    close(socket_fd);
    throw SocketBindingException();
  }
}

POG::Broadcaster::~Broadcaster() {
  close(socket_fd);
  while (!peers.empty()) {
    Peer *p = peers.front();
    peers.pop_front();
    delete p;
  }
}

void POG::Broadcaster::StartListening() {
  // Start listening
  if (listen(socket_fd, MAX_CONNECTIONS) == -1) {
    close(socket_fd);
    throw SocketListenException(this->port);
  }

  // Set a common client
  sockaddr_in6 client_address;
  socklen_t client_address_length = sizeof(client_address);

  // Start accept
  while (1) {
    /* Do TCP handshake with client */
    int client_sock_fd = accept(socket_fd, (struct sockaddr *)&client_address,
                                &client_address_length);

    if (client_sock_fd == -1) {
      throw SocketAcceptException();
    }

    // Check are we already know this client or not
    int found = -1;
    Peer *client;
    for (auto it = *peers.begin(); it != *peers.end() || found == 0; ++it) {
      found = socket_cmp(&it->address, &client_address);
      client = it;
    }

    // Add to the peers, cause it's a new client
    if (found == -1) {
      client = new Peer{};
      client->address = client_address;
      client->address_length = client_address_length;
      client->isOnline = true;
      client->port = client_address.sin6_port;
    }
  }
}

/**
 * Compare two sockaddrs.
 * @param left first address
 * @param right second address
 * @return 0 if left == right else -1
 **/
int POG::Broadcaster::socket_cmp(sockaddr_in6 *left, sockaddr_in6 *right) {
  socklen_t left_length = sizeof(left);
  socklen_t right_length = sizeof(right);

  if (left->sin6_family != right->sin6_family ||
      left->sin6_port != right->sin6_port ||
      left->sin6_addr.__in6_u.__u6_addr8 !=
          right->sin6_addr.__in6_u.__u6_addr8 ||
      left->sin6_addr.__in6_u.__u6_addr16 !=
          right->sin6_addr.__in6_u.__u6_addr16 ||
      left->sin6_addr.__in6_u.__u6_addr32 !=
          right->sin6_addr.__in6_u.__u6_addr32 ||
      left_length != right_length) {
    return -1;
  }

  return 0;
}