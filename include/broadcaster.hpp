/**
 * Author: Zarandi David (Azuwey)
 * Date: 03/08/2019
 * Source: https://gitlab.com/azuwey/protocol-off-grid
 * License: BSD 3-clause "New" or "Revised"
 **/

#ifndef PROTOCOL_OFF_GRID_BROADCASTER_HPP
#define PROTOCOL_OFF_GRID_BROADCASTER_HPP

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <cstdint>
#include <deque>
#include "common.hpp"
#include "exceptions.hpp"
#include "peer.hpp"

namespace POG {
class Broadcaster {
 private:
  int socket_s, socket_fd = 0;
  const int opt = OPT;
  std::uint_fast16_t port = DEFAULT_PORT;
  std::deque<POG::Peer*> peers;
  struct sockaddr_in6 address;

 public:
  Broadcaster();
  ~Broadcaster();

  void StartListening();
  int socket_cmp(sockaddr_in6 *left, sockaddr_in6 *right);
};
}  // namespace POG

#endif