/**
 * Author: Zarandi David (Azuwey)
 * Date: 03/08/2019
 * Source: https://gitlab.com/azuwey/protocol-off-grid
 * License: BSD 3-clause "New" or "Revised"
 **/

#ifndef PROTOCOL_OFF_GRID_PEER_HPP
#define PROTOCOL_OFF_GRID_PEER_HPP

#include <netinet/in.h>
#include "common.hpp"

namespace POG {
struct Peer {
  bool isOnline = DEFAULT_ONLINE_STATUS;
  uint_fast16_t port = DEFAULT_PORT;
  uint_fast16_t ttl = DEFAULT_TTL;
  struct sockaddr_in6 address;
  socklen_t address_length; 
};
}  // namespace POG

#endif