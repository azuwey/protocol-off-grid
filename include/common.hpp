/**
 * Author: Zarandi David (Azuwey)
 * Date: 03/09/2019
 * Source: https://gitlab.com/azuwey/protocol-off-grid
 * License: BSD 3-clause "New" or "Revised"
 **/

#ifndef PROTOCOL_OFF_GRID_COMMON_HPP
#define PROTOCOL_OFF_GRID_COMMON_HPP

#include <cstdint>

#define DEFAULT_PORT uint_fast16_t(8888)
#define DEFAULT_TTL uint_fast16_t(300)
#define DEFAULT_ONLINE_STATUS true
#define MAX_CONNECTIONS 100
#define OPT 1

#endif