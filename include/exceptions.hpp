/**
 * Author: Zarandi David (Azuwey)
 * Date: 03/09/2019
 * Source: https://gitlab.com/azuwey/protocol-off-grid
 * License: BSD 3-clause "New" or "Revised"
 **/

#ifndef PROTOCOL_OFF_GRID_EXCEPTIONS_HPP
#define PROTOCOL_OFF_GRID_EXCEPTIONS_HPP

#include <cstdint>
#include <exception>
#include <sstream>
#include "common.hpp"

namespace POG {
class FileDescriptorException : public std::exception {
 public:
  inline virtual const char* what() const throw() {
    return "Failed to init File Descriptor";
  }
};

class FileDescriptorBindingException : public std::exception {
 public:
  inline virtual const char* what() const throw() {
    return "Failed to bind the File Descriptor";
  }
};

class SocketBindingException : public std::exception {
 public:
  inline virtual const char* what() const throw() {
    return "Failed to bind the Socket";
  }
};

class SocketListenException : public std::exception {
 private:
  std::uint_fast16_t port;

 public:
  SocketListenException(std::uint_fast16_t port_) : port(port_) {}
  SocketListenException() : port(DEFAULT_PORT) {}

  inline virtual const char* what() const throw() {
    std::stringstream ss;
    ss << "Failed to Socket listen in the given " << port << " and address";
    return ss.str().c_str();
  }
};

class SocketAcceptException : public std::exception {
 public:
  inline virtual const char* what() const throw() {
    return "Failed to handshake with client";
  }
};
}  // namespace POG

#endif