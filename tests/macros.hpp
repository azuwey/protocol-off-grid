/**
 * Author: Zarandi David (Azuwey)
 * Date: 03/11/2019
 * Source: https://gitlab.com/azuwey/protocol-off-grid
 * License: BSD 3-clause "New" or "Revised"
 **/

#include <cppunit/TestCase.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/extensions/TestFactory.h>
#include <cppunit/ui/text/TestRunner.h>

#ifndef TEST_RUNNER_BEGIN
#define TEST_RUNNER_BEGIN int main() {
#endif

#ifndef TEST_RUNNER_END
#define TEST_RUNNER_END CppUnit::TextUi::TestRunner runner; \
  CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry(); \
  runner.addTest(registry.makeTest()); \
  runner.run(); \
  return (EXIT_SUCCESS); \
};
#endif