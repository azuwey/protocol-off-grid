/**
 * Author: Zarandi David (Azuwey)
 * Date: 03/11/2019
 * Source: https://gitlab.com/azuwey/protocol-off-grid
 * License: BSD 3-clause "New" or "Revised"
 **/

#include "peer.hpp"
#include "macros.hpp"

namespace POG {
class PeerTest : public CppUnit::TestCase {
 protected:
  CPPUNIT_TEST_SUITE(PeerTest);
  CPPUNIT_TEST(testEquality);
  CPPUNIT_TEST_SUITE_END();

 private:
  POG::Peer *p;

 public:
  PeerTest() : p(new POG::Peer()) {
    p->isOnline = false;
    p->port = 9999;
    p->ttl = 300;
  }

  ~PeerTest() { delete p; }

  void testEquality() {
    CPPUNIT_ASSERT(p->isOnline == false);
    CPPUNIT_ASSERT(p->port == 9999);
    CPPUNIT_ASSERT(p->ttl == 300);
  }
};
}  // namespace POG

TEST_RUNNER_BEGIN
  CPPUNIT_TEST_SUITE_REGISTRATION(POG::PeerTest);
TEST_RUNNER_END