/**
 * Author: Zarandi David (Azuwey)
 * Date: 03/11/2019
 * Source: https://gitlab.com/azuwey/protocol-off-grid
 * License: BSD 3-clause "New" or "Revised"
 **/

#include "broadcaster.hpp"
#include "macros.hpp"

namespace POG {
class BroadcasterTest : public CppUnit::TestCase {
 protected:
  CPPUNIT_TEST_SUITE(BroadcasterTest);
  CPPUNIT_TEST(testInitialization);
  CPPUNIT_TEST_SUITE_END();

 private:
  POG::Broadcaster *b;

 public:
  BroadcasterTest() {}

  void testInitialization() {
    CPPUNIT_ASSERT_ASSERTION_PASS(b = new Broadcaster());
    CPPUNIT_ASSERT_ASSERTION_PASS(delete b);
  }
};
}  // namespace POG

TEST_RUNNER_BEGIN
  CPPUNIT_TEST_SUITE_REGISTRATION(POG::BroadcasterTest);
TEST_RUNNER_END